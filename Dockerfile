FROM debian:bookworm 

RUN apt-get update 
RUN apt install git -y 

CMD [ "git", "--version" ] 